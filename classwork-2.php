<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Class work 02</title>
    <style>
        *{margin:0; padding:0;}
        h1, p{text-align: center;}
        .main{margin:0 auto; width:700px;}
        .box{background-color: #098021;width:155px;height: 155px;text-align:center;display:inline-block;margin:8px;vertical-align: middle;}
        .box p{font-size:18px;padding-top: 50%;margin-top:-9px;}
    </style>
</head>
<body>
<div class="main">
    <h1>CLASS WORK 2 – HTML/CSS </h1>
    <p>Create one square.html file to output as follows by using CSS: ( WITHOUT USING TABLE) </p>
    <div class="box"><p>1</p></div>
    <div class="box"><p>2</p></div>
    <div class="box"><p>3</p></div>
    <div class="box"><p>4</p></div>
    <div class="box"><p>5</p></div>
    <div class="box"><p>6</p></div>
    <div class="box"><p>7</p></div>
    <div class="box"><p>8</p></div>
    <div class="box"><p>9</p></div>
    <div class="box"><p>10</p></div>
    <div class="box"><p>11</p></div>
    <div class="box"><p>12</p></div>
    <div class="box"><p>13</p></div>
    <div class="box"><p>14</p></div>
    <div class="box"><p>15</p></div>
    <div class="box"><p>16</p></div>
</div>
</body>
</html>
